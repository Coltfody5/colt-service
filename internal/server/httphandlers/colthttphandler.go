package httphandlers

import (
	"colt/internal/server/commonhandlers"
	"colt/internal/util"
	"go.ftdr.com/go-utils/common/errors"
	"net/http"

	"go.ftdr.com/go-utils/common/logging"
	"golang.frontdoorhome.com/software/protos/go/coltpb"
)

// PostColtHTTPHandler
func PostColtHTTPHandler(config *util.RouterConfig) http.HandlerFunc {
	return func(w http.ResponseWriter, request *http.Request) {

		var response *coltpb.ColtResp
		ctx := request.Context()
		log := logging.GetTracedLogEntry(ctx)
		reqData := &coltpb.ColtReq{}

		marshalConfig, err := util.UnmarshalAndValidateRequest(request, reqData, log, config)
		if err != nil {
			response = commonhandlers.GetPostColtErrorResponse([]error{err})
			marshalConfig.WriteProtoOrJSONResponse(ctx, log, request, w, response, http.StatusBadRequest)
			return
		}

		// Log the request context
		// TODO : Add the fields as required

		response, err = commonhandlers.PostColtHandler(ctx, reqData, config)
		marshalConfig.WriteProtoOrJSONResponse(ctx, log, request, w, response, err.(*errors.ServerError).GetHTTPStatusCode())

	}
}
