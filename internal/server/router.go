package server

import (
	"colt/internal/config"
	"colt/internal/literals"
	"colt/internal/server/httphandlers"
	"colt/internal/testmiddleware"
	"colt/internal/util"
	"net/http"

	"github.com/gorilla/mux"
	"go.ftdr.com/go-utils/common/handlers"
	"go.ftdr.com/go-utils/common/middleware"
	instrumentation "go.ftdr.com/go-utils/instrumentation/v3"
)

const (
	protobufContentType = "application/x-protobuf"
	jsonContentType     = "application/json"
)

var (
	routeAcceptedContents = map[string][]string{
		literals.HealthcheckAPIName: {protobufContentType},
		literals.PostColtAPIName:    {protobufContentType, jsonContentType},
	}
)

// Router ...
type Router struct {
	*mux.Router
}

// NewRouter ...
func NewRouter() *Router {
	return &Router{mux.NewRouter()}
}

// InitializeRouter ...
func (r *Router) InitializeRouter(routerConfig *util.RouterConfig) {
	r.initializeMiddleware(routerConfig.WebServerConfig)
	r.initializeRoutes(routerConfig)
}

// initializeRoutes ...
func (r *Router) initializeRoutes(routerConfig *util.RouterConfig) {
	s := (*r).PathPrefix(routerConfig.WebServerConfig.RoutePrefix).Subrouter()

	s.HandleFunc(literals.HealthcheckUri,
		handlers.HealthCheckHandler(routerConfig.CIConfig.EnvironmentName,
			routerConfig.CIConfig.CommitShortSHA, routerConfig.DependencyURL)).
		Methods(http.MethodGet).
		Name(literals.HealthcheckAPIName)
	s.HandleFunc(literals.PostColtURI,
		httphandlers.PostColtHTTPHandler(routerConfig)).
		Methods(http.MethodOptions, http.MethodPost).
		Name(literals.PostColtAPIName)
}

// initializeMiddleware ..
func (r *Router) initializeMiddleware(serverConfig *config.WebServerConfig) {
	r.Use(middleware.TraceMiddleware)
	r.Use(instrumentation.Middleware)
	vmw := middleware.ValidatorMiddleWare{}
	vmw.SetAcceptedContents(routeAcceptedContents)
	r.Use(vmw.Middleware)
	r.Use(middleware.ReadReqMiddleware)
	if serverConfig.EnableTestMiddleware {
		r.Use(testmiddleware.Init(serverConfig.ResponseDirPath).Middleware)
	}
	r.Use(middleware.ClientInjectorMiddleware("", "", "", "", 50051))
	r.Use(middleware.Recovery)
}
