package commonhandlers

import (
	"colt/internal/services"
	"colt/internal/util"
	"context"

	"golang.frontdoorhome.com/software/protos/go/coltpb"
)

func PostColtHandler(ctx context.Context, req *coltpb.ColtReq, config *util.RouterConfig) (*coltpb.ColtResp, error) {

	var response *coltpb.ColtResp

	service := services.GetPostColtService()

	errs := service.ValidateRequest(ctx, req)
	if errs != nil {
		response = GetPostColtErrorResponse(errs)
		return response, errs[0]
	}

	responseMsg, processErr := service.ProcessRequest(ctx, req)
	if processErr != nil {
		response = GetPostColtErrorResponse([]error{processErr})
	} else {
		response = responseMsg
	}

	return response, processErr
}

func GetPostColtErrorResponse(errs []error) *coltpb.ColtResp {
	errProto := util.ServerToProtoError(errs)
	response := &coltpb.ColtResp{
		Errors: errProto,
	}

	return response
}
