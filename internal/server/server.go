package server

import (
	"colt/internal/config"
	"colt/internal/services"
	"colt/internal/util"
	"net"
	"net/http"
	"net/url"

	"github.com/soheilhy/cmux"
	"go.ftdr.com/go-utils/common/logging"
	commonServices "go.ftdr.com/go-utils/common/services"
	instrumentation "go.ftdr.com/go-utils/instrumentation/v3"
	"google.golang.org/grpc"
)

const (
	dependencyURL = "https://gitlab.com/ftdr/software/colt/"
)

//Server ...
type Server struct {
	Configuration *config.WebServerConfig
	Router        *Router
}

//NewServer ...
func NewServer(config *config.WebServerConfig) *Server {
	server := &Server{
		Configuration: config,
		Router:        NewRouter(),
	}

	return server
}

//RunServer ...
func RunServer() (err error) {
	webServerConfig, logCfg, ciCfg, err := config.FromEnv()
	if err != nil {
		return err
	}

	err = logging.Initialize(logCfg)
	if err != nil {
		return err
	}

	// initialize instrumentation
	instrumentation.Initialize(webServerConfig.Instrumentation)
	defer instrumentation.Shutdown()

	baseURL, err := url.Parse(dependencyURL)
	if err != nil {
		logging.Log.WithError(err).Error()
		return err
	}

	routerConfigs := util.RouterConfig{
		WebServerConfig: webServerConfig,
		CIConfig:        ciCfg,
		DependencyURL:   *baseURL,
	}

	// Initializing dao
	initDao(&routerConfigs)

	// Initializing services
	initServices(&routerConfigs)

	server := NewServer(webServerConfig)
	server.Router.InitializeRouter(&routerConfigs)

	cors := commonServices.CORSSetup()

	l, err := net.Listen("tcp", ":"+webServerConfig.Port)
	if err != nil {
		return err
	}

	// Create a cmux.
	cmuxServer := cmux.New(l)

	// Match connections in order:
	grpcListener := cmuxServer.Match(cmux.HTTP2())
	httpListener := cmuxServer.Match(cmux.HTTP1())

	grpcServer := grpc.NewServer()
	registerGRPCServers(grpcServer, &routerConfigs)

	httpServer := &http.Server{
		Addr:    ":" + webServerConfig.Port,
		Handler: cors.Handler(server.Router),
	}

	// Use the muxed listeners for your servers.
	go grpcServer.Serve(grpcListener)
	go httpServer.Serve(httpListener)

	logging.Log.Infof("Starting HTTP and GRPC server on port %s", webServerConfig.Port)
	// Start serving!
	cmuxServer.Serve()

	return nil
}

// Initializes dao
func initDao(routerConf *util.RouterConfig) {
}

// Initializes services by calling respective Init method
func initServices(routerConf *util.RouterConfig) {
	services.InitPostColtService(routerConf)
}

func registerGRPCServers(grpcServer *grpc.Server, routerConfig *util.RouterConfig) {

}
