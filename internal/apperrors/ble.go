package apperrors

import (
	"colt/internal/literals"
	"net/http"

	"go.ftdr.com/go-utils/common/errors"
)

var (

	// ProtoUnmarshalError Failed to unmarshal byte array to proto struct
	ProtoUnmarshalError = errors.ServerError{
		Message:          literals.ProtoUnmarshalError,
		Code:             literals.AppPrefix + "_BLE_0001",
		HTTPResponseCode: http.StatusInternalServerError,
	}
	// ProtoMarshalError Failed to marshal proto struct to byte array
	ProtoMarshalError = errors.ServerError{
		Message:          literals.ProtoMarshalError,
		Code:             literals.AppPrefix + "_BLE_0002",
		HTTPResponseCode: http.StatusInternalServerError,
	}
	// MissingUserID Logged-in user ID is either missing or passed as empty value in request data
	MissingUserID = errors.ServerError{
		Message:          literals.MissingUserID,
		Code:             literals.AppPrefix + "_BLE_0003",
		HTTPResponseCode: http.StatusBadRequest,
	}
	// InvalidRequestDataError Invalid request data
	InvalidRequestDataError = errors.ServerError{
		Message:          literals.InvalidRequestDataError,
		Code:             literals.AppPrefix + "_BLE_0004",
		HTTPResponseCode: http.StatusBadRequest,
	}
	// Base64DecodeError Failed to decode base64 url query param
	Base64DecodeError = errors.ServerError{
		Message:          literals.Base64DecodeError,
		Code:             literals.AppPrefix + "_BLE_0005",
		HTTPResponseCode: http.StatusBadRequest,
	}
	// EncoderError Could not encode the response
	EncoderError = errors.ServerError{
		Message:          literals.EncoderError,
		Code:             literals.AppPrefix + "_BLE_0006",
		HTTPResponseCode: http.StatusInternalServerError,
	}
	// RequestBodyReadError Failed to read data from the request body
	RequestBodyReadError = errors.ServerError{
		Message:          literals.RequestBodyReadError,
		Code:             literals.AppPrefix + "_BLE_0007",
		HTTPResponseCode: http.StatusInternalServerError,
	}
	// ProtoToMapConversionError Unable to convert proto message to map
	ProtoToMapConversionError = errors.ServerError{
		Message:          literals.ProtoToMapConversionError,
		Code:             literals.AppPrefix + "_BLE_0008",
		HTTPResponseCode: http.StatusInternalServerError,
	}
	// InvalidTenant Tenant is invalid
	InvalidTenant = errors.ServerError{
		Message:          literals.InvalidTenant,
		Code:             literals.AppPrefix + "_BLE_0009",
		HTTPResponseCode: http.StatusBadRequest,
	}
	// JSONUnmarshalError Failed to unmarshal struct to JSON string
	JSONUnmarshalError = errors.ServerError{
		Message:          literals.JSONUnmarshalError,
		Code:             literals.AppPrefix + "_BLE_0010",
		HTTPResponseCode: http.StatusInternalServerError,
	}
	// MissingUserRole User Role is either missing or passed as empty value in request data
	MissingUserRole = errors.ServerError{
		Message:          literals.MissingUserRole,
		Code:             literals.AppPrefix + "_BLE_0011",
		HTTPResponseCode: http.StatusBadRequest,
	}
	// APINotSupportedError API is not supported
	APINotSupportedError = errors.ServerError{
		Message:          literals.APINotSupportedError,
		Code:             literals.AppPrefix + "_BLE_0012",
		HTTPResponseCode: http.StatusInternalServerError,
	}
	// InvalidUserRole Operation not allowed for user role
	InvalidUserRole = errors.ServerError{
		Message:          literals.InvalidUserRole,
		Code:             literals.AppPrefix + "_BLE_0013",
		HTTPResponseCode: http.StatusBadRequest,
	}
	// UUIDParseFailedError Failed to parse uuid
	UUIDParseFailedError = errors.ServerError{
		Message:          literals.UUIDParseFailedError,
		Code:             literals.AppPrefix + "_BLE_0014",
		HTTPResponseCode: http.StatusBadRequest,
	}
	// FileReadError Unable to read file
	FileReadError = errors.ServerError{
		Message:          literals.FileReadError,
		Code:             literals.AppPrefix + "_BLE_0015",
		HTTPResponseCode: http.StatusInternalServerError,
	}
	// InvalidUserID User ID must be a valid UUID
	InvalidUserID = errors.ServerError{
		Message:          literals.InvalidUserID,
		Code:             literals.AppPrefix + "_BLE_0016",
		HTTPResponseCode: http.StatusBadRequest,
	}
	// MissingTenant Tenant is either missing or passed as empty value in request data
	MissingTenant = errors.ServerError{
		Message:          literals.MissingTenant,
		Code:             literals.AppPrefix + "_BLE_0017",
		HTTPResponseCode: http.StatusBadRequest,
	}
)
