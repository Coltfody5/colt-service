package services

import (
	"context"

	"golang.frontdoorhome.com/software/protos/go/coltpb"

	"sync"

	"colt/internal/util"
)

// PostColtService ...
type PostColtService interface {
	ValidateRequest(ctx context.Context, req *coltpb.ColtReq) (
		validationErrors []error)
	ProcessRequest(ctx context.Context, req *coltpb.ColtReq) (
		*coltpb.ColtResp, error)
}

var postColtSvcStruct PostColtService
var postColtServiceOnce sync.Once

// postColtService ...
type postColtService struct {
	config *util.RouterConfig
}

// InitPostColtService ...
func InitPostColtService(config *util.RouterConfig) PostColtService {
	postColtServiceOnce.Do(func() {
		postColtSvcStruct = &postColtService{config: config}
	})
	return postColtSvcStruct
}

// GetPostColtService ...
func GetPostColtService() PostColtService {
	if postColtSvcStruct == nil {
		panic("PostColtService not initialized")
	}
	return postColtSvcStruct
}

// ValidateRequest ...
func (service *postColtService) ValidateRequest(ctx context.Context, req *coltpb.ColtReq) (
	validationErrors []error) {
	// TODO: Add validation logic here
	return nil
}

// ProcessRequest ...
func (service *postColtService) ProcessRequest(ctx context.Context, req *coltpb.ColtReq) (*coltpb.ColtResp, error) {
	resp := coltpb.ColtResp{
		Colt:   "dixon",
	}
	return &resp, nil
}
