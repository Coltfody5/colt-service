package util

import (
	"colt/internal/config"
	"colt/internal/literals"
	"encoding/base64"
	"errors"
	"net/http/httptest"
	"testing"

	"bou.ke/monkey"
	"github.com/google/uuid"
	"github.com/mitchellh/mapstructure"
	"github.com/stretchr/testify/assert"
	"go.ftdr.com/go-utils/common/logging"
)

func patchBase64Decode() {
	monkey.Patch((*base64.Encoding).Decode, func(*base64.Encoding, []byte, []byte) (int, error) {
		return 0, errors.New("mocked-base64-decode-error")
	})
}

func unpatchBase64Decode() {
	monkey.Unpatch((*base64.Encoding).Decode)
}

func patchMapStructureDecode() {
	monkey.Patch(mapstructure.Decode, func(interface{}, interface{}) error {
		return errors.New("mocked-map-structure-decode-error")
	})
}

func unpatchMapStructureDecode() {
	monkey.Unpatch(mapstructure.Decode)
}

func getRouterConfig() *RouterConfig {
	conf, _, _, err := config.FromEnv()
	if err != nil {
		logging.Log.Error("Error while getting router info : ", err)
	}
	return &RouterConfig{WebServerConfig: conf}
}

func TestIsValidUUID_InvalidRequest_returnedFalse(t *testing.T) {
	id := literals.EmptyUUID
	result := IsValidUUID(id)
	assert.False(t, result, nil)
}

func TestIsValidUUID_EmptyString_returnedFalse(t *testing.T) {
	id := ""
	result := IsValidUUID(id)
	assert.False(t, result, nil)
}
func TestIsValidUUID_validRequest_returnedTrue(t *testing.T) {
	id := uuid.New().String()
	result := IsValidUUID(id)
	assert.True(t, result, nil)
}

func TestSetResponseHeader_ValidInput_ReturnsNil(t *testing.T) {
	responseWriter := httptest.NewRecorder()
	SetResponseHeader(responseWriter)

	assert.Equal(t, "application/x-protobuf", responseWriter.Header().Get("Content-Type"))
	assert.Equal(t, "default-src 'self';", responseWriter.Header().Get("Content-Security-Policy"))
	assert.Equal(t, "max-age="+secondsInOneYear, responseWriter.Header().Get("Strict-Transport-Security"))
}
