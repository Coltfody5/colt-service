#!/bin/bash
cd "$(dirname "$0")"

covs=`grep -o '>.*%)</option>' coverage*.out.html | grep -v "_mock" | grep -v "mockservice" | grep -v "testutils" | grep -v "test-utils"`
covs="${covs//<\/option>}" # strip away end tag
covs="${covs//>}" # strip away the inital >

echo -e "\nTEST COVERAGE SUMMARY\n\nPer file coverage:"
echo "$covs"

percs=`grep -o '[0-9\.]*%' <<< "$covs"`
count=`awk -F"%" '{print NF-1}' <<< ${percs}`
percs="${percs//%}" # strip away the %
avg=`awk '{ SUM += $1} END { print SUM/NR }' <<< "$percs"`
printf "\nAverage test coverage: %.2f" $avg
echo -e "%"
