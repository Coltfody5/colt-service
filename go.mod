module colt

go 1.13

require (
	bou.ke/monkey v1.0.2
	github.com/google/uuid v1.3.0
	github.com/gorilla/mux v1.8.0
	github.com/joho/godotenv v1.3.0
	github.com/kelseyhightower/envconfig v1.4.0
	github.com/mitchellh/mapstructure v0.0.0-20170523030023-d0303fe80992
	github.com/sirupsen/logrus v1.7.0
	github.com/soheilhy/cmux v0.1.5
	github.com/stretchr/testify v1.7.0
	go.ftdr.com/go-utils/common v0.4.1
	go.ftdr.com/go-utils/instrumentation/v3 v3.0.1
	golang.frontdoorhome.com/software/protos v1.0.1979
	google.golang.org/grpc v1.44.0
	google.golang.org/protobuf v1.27.1
)

replace golang.frontdoorhome.com/software/protos => /Users/Colt.Dixon/Code/protos
